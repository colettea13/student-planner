import java.awt.*;

public class Project extends Assignment {
   
   public Project (String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //reminderDate = getReminderDate();
      //type = "Project";
      //makeReminder();
   }
   
   public Color getColor() {
      //return new Color(89, 235, 49);
      return new Color(255, 178, 102);
   }
   
   public String setType() {
      return "Project";
  }
   
   public Day getReminderDate() {
      Day d = dueDate;
      d = new Day(d.subtract(3)[0], d.subtract(3)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String toString() {
      return "Project " + name + " due";
   }
}