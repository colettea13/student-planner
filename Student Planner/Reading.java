import java.awt.*;

public class Reading extends Assignment{

   public int chapters;

   public Reading(String name, Day dueDate, Class subject, int c) {
      super(name, dueDate, subject);
      chapters = c;
      extra = c + "";
   }
   
   public Color getColor() {
      return new Color(173, 90, 0);
   }
   
   public String setType() {
      return "Reading";
  }

   public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(chapters)[0], d.subtract(chapters)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String toString() {
      return "Reading " + name + " due";
  }

}
