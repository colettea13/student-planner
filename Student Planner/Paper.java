import java.awt.*;

public class Paper extends Assignment {
  
   public Paper (String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //this.reminderDate = getReminderDate();
      //type = "Paper";
      //makeReminder();
   }
   
   public Color getColor() {
      //return new Color(245, 102, 255);
      return new Color(255, 102, 178);
   }
   
   public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(3)[0], d.subtract(3)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String setType() {
      return "Paper";
  }
   
   public String toString() {
      return "Paper " + name + " due";
   }
}