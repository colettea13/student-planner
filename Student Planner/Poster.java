
public class Poster extends Project {
   
   public Poster (String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
   }
   
   public String setType() {
      return "Poster";
  }
   
   public String toString() {
      return "Poster " + name + " due";
   }
}
