import java.awt.*;

public class Event extends Assignment {
   
   public Event (String name, Day dueDate, Class subject, Time startTime) {
      super(name, dueDate, subject);
      reminderDate = dueDate;
      reminderTime = getReminderTime(startTime);
      makeReminder();
      extra = startTime.hour + "/" + startTime.minute + "/" + startTime.mornNight;
   }
   
   public String setType() {
      return "Event";
  }
  
  public Color getColor() {
   return new Color(208, 78, 237);
  }
   
   public String toString() {
      return name;
   }
   
   public Time getReminderTime(Time startTime){
      return new Time(startTime.absTime - 1);
   }
}
