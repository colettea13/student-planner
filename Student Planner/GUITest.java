import java.util.*;
import java.text.*;
import java.awt.*;
import java.applet.*;

public class GUITest extends Applet {
      ArrayList<Day[]> schoolYear;
      ArrayList<Class> classes;
      ArrayList<Assignment> assigns;
      Scanner input;
      DrawCalendar month, week, thisDay;
   
   public void init() {
      schoolYear = new ArrayList<Day[]>();
      classes = new ArrayList<Class>();
      assigns = new ArrayList<Assignment>();
      input = new Scanner(System.in);
      Day endDate = new Day(6, 16, 2015);
      DateFormat dateFormat = new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
      Calendar cal = Calendar.getInstance();
      String dateString = dateFormat.format(cal.getTime());
      Day today = new Day (Integer.parseInt(dateString.substring(9, 11)),
                           Integer.parseInt(dateString.substring(12, 14)),
                           Integer.parseInt(dateString.substring(4, 8)));
      Day calStart = new Day(today.firstDayOfWeek[0], today.firstDayOfWeek[1]);
      Day calEnd = new Day(endDate.firstDayOfWeek[0], endDate.firstDayOfWeek[0]);
      calEnd  = new Day(calEnd.add(6)[0], calEnd.add(6)[0]);
      Day count = new Day(calStart.absPos, calStart.year);
      for (int i = 0; i <= calStart.daysTo(calEnd) / 7; i ++) {
         Day[] week = new Day[7];
         for (int j = 0; j < 7; j++) {
             week[j] = count;
             count = new Day(count.dayAfter[0], count.dayAfter[1]);
         }
         schoolYear.add(week);
      }
      
      classes.add(new Class("Pre-Calc", new Time(7, 50, true), new Time(8, 45, true)));
      classes.add(new Class("LA10", new Time(8, 50, true), new Time(9, 45, true)));
      classes.add(new Class("APHG", new Time(9, 50, true), new Time(10, 45, true)));
      classes.add(new Class("APCS", new Time(11, 25, true), new Time(12, 20, false)));
      classes.add(new Class("Bio", new Time(12, 25, false), new Time(1, 20, false)));
      classes.add(new Class("French", new Time(1, 25, false), new Time(2, 20, false)));
      
      assigns.add(new ArtProject("collage on imagery from book quote", new Day(5, 20, 2015), classes.get(1)));
      assigns.add(new Assignment("got to help Helena w/ poem", new Day(5, 21, 2015), classes.get(1)));
      assigns.add(new Essay("compare/contrast - SH5 and Allegory Of The Cave Stephen Dunn", new Day(5, 20, 2015), classes.get(1), 2));
      assigns.add(new Event("performing in assembly", new Day(5, 22, 2015), classes.get(2), new Time(10, 00, true)));
      assigns.add(new FieldTrip("going to microsoft, bring notebook", new Day(5, 26, 2015), classes.get(3)));
      assigns.add(new Final("math final", new Day(5, 29, 2015), classes.get(0)));
      assigns.add(new Homework("math problems 1-5", new Day(5, 18, 2015), classes.get(0)));
      assigns.add(new Lab("cs lab w/ graphics", new Day(5, 19, 2015), classes.get(3)));
      assigns.add(new Paper("write up for movie", new Day(5, 25, 2015), classes.get(2)));
      assigns.add(new Poem("must be inspired by SH5 quote", new Day(5, 25, 2015), classes.get(1)));
      assigns.add(new Poster("grasshopper anatomy", new Day(5, 28, 2015), classes.get(4)));
      assigns.add(new Project("create star - Little Prince", new Day(5, 21, 2015), classes.get(5)));
      assigns.add(new Quiz("on subjonctif", new Day(5, 27, 2015), classes.get(5)));
      assigns.add(new Reading("SH5 chap 7 & 8", new Day(5, 29, 2015), classes.get(1), 2));
      assigns.add(new ResearchPaper("why plants grow - cr + photo", new Day(5, 28, 2015), classes.get(4)));
      assigns.add(new ShortStory("10 vignettes from vacation", new Day(5, 26, 2015), classes.get(1), 3));
      assigns.add(new SlideShow("teach sector model", new Day(5, 20, 2015), classes.get(2), 6));
      assigns.add(new Test("test on city models", new Day(5, 27, 2015), classes.get(2)));
      assigns.add(new Vocab("words on happiness", new Day(5, 22, 2015), classes.get(1), 20));
      
      Collections.sort(classes);
      Collections.sort(assigns);
      week = new DrawCalendar(schoolYear.get(1), assigns, classes);
      thisDay = new DrawCalendar(schoolYear.get(1)[3], assigns);
   }
   
   public void paint(Graphics g) {
      resize(1000, 500);
      week.drawWeekGrid(g);
      //thisDay.drawDay(g);
   }
}

/*
//The task which you want to execute
private static class MyTimeTask extends TimerTask
{

    public void run()
    {
        //write your code here
    }
}

public static void main(String[] args) {

    //the Date and time at which you want to execute
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = dateFormatter .parse("2012-07-06 13:05:45");

    //Now create the time and schedule it
    Timer timer = new Timer();

    //Use this if you want to execute it once
    timer.schedule(new MyTimeTask(), date);

    //Use this if you want to execute it repeatedly
    //int period = 10000;//10secs
    //timer.schedule(new MyTimeTask(), date, period );
}
*/