import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.io.*;

public class JTester {
   
   public static void main(String[] args) {
      JTester test = new JTester();
   }
   
   ArrayList<Day[]> schoolYear;
   ArrayList<Class> classes;
   ArrayList<Assignment> assigns;
   DrawCalendar week, thisDay;
   JFrame frame;
   int showWeek;
   Day showDay, calStart, calEnd;
   Action toWeek, toDay, switchWeek, switchDay, toAssignAdder, toClass, toYear, changeArrayList, getAssignType;
   JPanel botNav, topTit, cardPanel, assignTypePanel;
   CardLayout calCards, bNCards, tTCards;
   ArrayList<String> calCardsNames, bNCardsNames, tTCardsNames;
   boolean dayAssign;
   String thisAssignType;
   String[] basicAssignTypes = {"Assignment", "Event", "Field Trip", "Homework", "Lab", "Paper", "Project", "Reading", "Test"};
   String[] paperTypes = {"Paper", "Essay", "Poem", "Research Paper", "Short Story"};
   String[] projectTypes = {"Project", "Art Project", "Poster", "Slide Show"};
   String[] testTypes = {"Test", "Final", "Quiz", "Vocab"};
   JTextField assignDescrip, dueDate, className, addInfo, startTime, endTime, startDate, endDate, weeksAdded;
   JComboBox classOptions, assignType, paperType, projectType, testType;
   JLabel des, dD, subj, ty;
   PrintWriter writer;
   
   public JTester() {
      schoolYear = new ArrayList<Day[]>();
      classes = new ArrayList<Class>();
      assigns = new ArrayList<Assignment>();
      String userName = "";
      while (userName != null && userName.length() <= 0) {
         userName = (String)JOptionPane.showInputDialog("Please enter your name").toLowerCase();
      }
      String schoolEnd = "";
      DateFormat dateFormat = new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
      Calendar cal = Calendar.getInstance();
      String dateString = dateFormat.format(cal.getTime());
      Day today = new Day (Integer.parseInt(dateString.substring(9, 11)),
                           Integer.parseInt(dateString.substring(12, 14)),
                           Integer.parseInt(dateString.substring(4, 8)));
      boolean isPreviousUser = false;
      try {
         File userInfo = new File(userName + ".txt");
         Scanner inputFile = new Scanner(userInfo);
         Scanner startEnd = new Scanner(inputFile.nextLine());
         String[] sD = startEnd.next().split("/");
         calStart = new Day(Integer.parseInt(sD[0]), Integer.parseInt(sD[1]), Integer.parseInt(sD[2]));
         schoolEnd = startEnd.next();
         Scanner getClasses = new Scanner(inputFile.nextLine());
         while (getClasses.hasNext()) {
            String[] thisClass = getClasses.next().split("`");
            String na = splitRecomb(thisClass[0].split("~"), " ");
            String[] s = thisClass[1].split("/");
            String[] e = thisClass[2].split("/");
            Time sT = new Time(Integer.parseInt(s[0]), Integer.parseInt(s[1]), s[2]);
            Time eT = new Time(Integer.parseInt(e[0]), Integer.parseInt(e[1]), e[2]);
            classes.add(new Class(na, sT, eT));
         }
         Scanner getAssigns = new Scanner(inputFile.nextLine());
         while (getAssigns.hasNext()) {
            String[] tA = getAssigns.next().split("`");
            String ty = splitRecomb(tA[0].split("~"), " ");
            String na = splitRecomb(tA[1].split("~"), " ");
            String c = splitRecomb(tA[3].split("~"), " ");
            String[] d = tA[2].split("/");
            Day da = new Day(Integer.parseInt(d[0]), Integer.parseInt(d[1]), Integer.parseInt(d[2]));
            Class cl = findClass(c);
            assigns.add(getType(ty, na, da, cl, tA[4]));
         }
         isPreviousUser = true;
      } catch (FileNotFoundException e) {
         while (schoolEnd != null && schoolEnd.length() <= 0) {
            schoolEnd = (String)JOptionPane.showInputDialog("Please enter your school end date in the format mm/dd/yyyy");
         }
         calStart = new Day(today.firstDayOfWeek[0], today.firstDayOfWeek[1]);
      }
      
      String[] endArr = schoolEnd.split("/");
      Day endDate = new Day(Integer.parseInt(endArr[0]), Integer.parseInt(endArr[1]), Integer.parseInt(endArr[2]));
      calEnd = new Day(endDate.firstDayOfWeek[0], endDate.firstDayOfWeek[0]);
      calEnd  = new Day(calEnd.add(6)[0], calEnd.add(6)[0]);
      makeYear();
      
      Collections.sort(classes);
      Collections.sort(assigns);
      
      frame = new JFrame();
      frame.setSize(new Dimension(1500, 500));
      frame.setTitle("Student Planner");
      frame.setLayout(new BorderLayout());
      
      switchWeek = new Switch();
      switchDay = new Switch();
      toDay = new ViewTo("day");
      toWeek = new ViewTo("week");
      toAssignAdder = new ViewTo("assignAdder");
      toClass = new ViewTo("class");
      toYear = new ViewTo("year");
      changeArrayList = new AddOrRemove();
      getAssignType = new AddChoice();
            
      calCards = new CardLayout(); 
      calCardsNames = new ArrayList<String>();
      cardPanel = new JPanel(); 
      cardPanel.setLayout(calCards); 
      
      bNCards = new CardLayout(); 
      bNCardsNames = new ArrayList<String>();
      botNav = new JPanel(); 
      botNav.setLayout(bNCards); 
      
      tTCards = new CardLayout();
      tTCardsNames = new ArrayList<String>(); 
      topTit = new JPanel(); 
      topTit.setLayout(tTCards); 
      
      if (isPreviousUser) {
         drawWeekCal(0);
      } else {
         drawClassAdder();
      }
      
      Scanner input = new Scanner(System.in);
      System.out.print("Type 'cal' to reopen calendar, type 'close' to save your information and exit the program: ");
      String instruction = input.next();
      while (!instruction.equalsIgnoreCase("close")) {
         frame = new JFrame();
         frame.setSize(new Dimension(1500, 500));
         frame.setTitle("Student Planner");
         frame.setLayout(new BorderLayout());
         drawWeekCal(0);
         System.out.print("Type 'cal' to reopen calendar, type 'close' to save your information and exit the program: ");
         instruction = input.next();
      }
      try {
         writer = new PrintWriter(userName + ".txt");
      } catch (FileNotFoundException e) {
         System.out.println("File not found/couldn't be created");
      }
      writer.println(calStart + " " + calEnd);
      for (Class c : classes) {
         String na = splitRecomb(c.name.split(" "), "~");
         writer.print(na + "`" + 
               (c.startTime.hour + "/" + c.startTime.minute + "/" + c.startTime.mornNight) + "`" + 
               (c.endTime.hour + "/" + c.endTime.minute + "/" + c.endTime.mornNight));
         if (!c.equals(classes.get(classes.size()-1))) {
            writer.print(" ");
         }
      }
      writer.println();
      for (Assignment a : assigns) {
         String ty = splitRecomb(a.type.split(" "), "~");
         String na = splitRecomb(a.name.split(" "), "~");
         String cl = splitRecomb(a.subject.name.split(" "), "~");
         String ex = splitRecomb(a.extra.split(" "), "~");
         writer.print(ty + "`" + na + "`" + a.dueDate.toString() + "`" + cl + "`" + ex + " ");
         if (!a.equals(assigns.get(assigns.size()-1))) {
            writer.print(" ");
         }
      }
      writer.close();
      System.exit(0);
   }
   
   public void drawWeekCal(int w) {
      showWeek = w;
      JPanel thisWeekCal = new JPanel(new GridLayout(classes.size() + 1, 8));
      week = new DrawCalendar(schoolYear.get(w), assigns, classes);
      for (int i = 0; i < week.weekGrid.length; i++) {
         for (int j = 0; j < week.weekGrid[i].length; j++) {
            JButton b = new JButton(week.weekGrid[i][j]);
            b.setFont(new Font("Times New Roman", Font.PLAIN, 15));
            if (week.isToday[i][j]) {
               if (i == 0 || i == 1) {
                  b.setBorder(BorderFactory.createMatteBorder(2, 2, 0, 2, Color.BLACK));
               } else if (i == week.weekGrid.length - 1) {
                  b.setBorder(BorderFactory.createMatteBorder(1, 2, 2, 2, Color.BLACK));
               } else {
                  b.setBorder(BorderFactory.createMatteBorder(1, 2, 0, 2, Color.BLACK));
               }
            }
            if (j == 0) {
               b.setForeground(new Color(255, 255, 255));
            }
            b.setBackground(week.weekColor[i][j]);
            b.setMargin(new Insets(5,5,5,5));
            if (i > 0 && j > 0) {
               b.setActionCommand(week.week[j-1].toString());
               b.addActionListener(toDay);
            }
            thisWeekCal.add(b);
         }
      }
      cardPanel.add(thisWeekCal, week.week[0] + " - " + week.week[6]);
      calCards.show(cardPanel, week.week[0] + " - " + week.week[6]);
      frame.add(cardPanel, BorderLayout.CENTER);
      
      drawNav("Previous Week", "Next Week", week.week[0] + " - " + week.week[6], switchWeek, switchWeek);
      drawTit("Week of " + week.week[0] + " - " + week.week[6], "Add Assignment", toAssignAdder);

      frame.setVisible(true);
   }
   
   public void drawDayCal(Day d) {
      showDay = d;
      thisDay = new DrawCalendar(d, assigns);
      JPanel thisDayCal = new JPanel(new GridLayout(thisDay.dayInfo.size()-1, 1));
      for (int i = 1; i < thisDay.dayInfo.size(); i++) {
         JLabel b = new JLabel(thisDay.dayInfo.get(i));
         b.setFont(new Font("Times New Roman", Font.PLAIN, 25));
         Color c = thisDay.dayColor.get(i);
         if (thisDay.dayIsToday) {
            Border thickBorder = new LineBorder(Color.BLACK, 3);
            b.setBorder(thickBorder);
         }
         b.setBackground(c);
         b.setOpaque(true);
         //b.setForeground(new Color(255-c.getRed(), 255-c.getBlue(), 255-c.getGreen()));
         b.setHorizontalAlignment(SwingConstants.CENTER);
         thisDayCal.add(b);
      }
      cardPanel.add(thisDayCal, d.toString());
      calCards.show(cardPanel, d.toString());
      frame.add(cardPanel, BorderLayout.CENTER);
      
      drawNav("Previous Day", "Next Day", showDay.toString(), switchDay, switchDay);
      
      JPanel title = new JPanel(new GridLayout(1, 3));
      JLabel t = new JLabel("Assignments due on " + d.toString());
      t.setHorizontalAlignment(SwingConstants.CENTER);
      t.setFont(new Font("Times New Roman", Font.PLAIN, 30));
      t.setBackground(new Color(51, 153, 255));
      t.setOpaque(true);
      title.add(t);
      JButton goToAssignAdder = new JButton("Add Assignment");
      goToAssignAdder.setFont(new Font("Times New Roman", Font.PLAIN, 20));
      goToAssignAdder.setBackground(new Color(102, 255, 102));
      goToAssignAdder.addActionListener(toAssignAdder);
      title.add(goToAssignAdder);
      JButton backToWeek = new JButton("Return to week");
      backToWeek.setFont(new Font("Times New Roman", Font.PLAIN, 20));
      backToWeek.setBackground(new Color(153, 204, 255));
      dayAssign = true;
      backToWeek.addActionListener(toWeek);
      title.add(backToWeek);
      topTit.add(title, d.toString());
      tTCards.show(topTit, d.toString());
      frame.add(topTit, BorderLayout.NORTH);
      frame.setVisible(true);
   }
   
   public void drawAssignAdder() {
      int count = 4;
      GridLayout aA = new GridLayout(9, 1);
      JPanel assignAdder = new JPanel(aA);
      des = new JLabel("Assignment description: ");
      assignAdder.add(des);
      assignDescrip = new JTextField();
      assignAdder.add(assignDescrip);
      subj = new JLabel("Class assignment is for: ");
      assignAdder.add(subj);
      classOptions = new JComboBox(classes.toArray(new Class[classes.size()]));
      assignAdder.add(classOptions);
      dD = new JLabel("Assignment due date in the format mm/dd/yyyy: ");
      assignAdder.add(dD);
      dueDate = new JTextField();
      assignAdder.add(dueDate);
      ty = new JLabel("Type of assignment(lab, paper, field-trip, event, reading, test, project, or hw): ");
      assignAdder.add(ty);
      assignTypePanel = new JPanel(new GridLayout(1, 3));
      assignType = new JComboBox(basicAssignTypes);
      paperType = new JComboBox(paperTypes);
      projectType = new JComboBox(projectTypes);
      testType = new JComboBox(testTypes);
      addInfo = new JTextField();
      assignType.addActionListener(getAssignType);
      assignTypePanel.add(assignType);
      assignAdder.add(assignTypePanel);
      JButton addAssign = new JButton("Add Assignment");
      addAssign.addActionListener(changeArrayList);
      assignAdder.add(addAssign);
      thisAssignType = "Assignment";
            
      cardPanel.add(assignAdder, "hi");
      calCards.show(cardPanel, "hi");
      frame.add(cardPanel, BorderLayout.CENTER);
      
      drawNav("Add or Remove Class", "Change School Year", "Add Assignment", toClass, toYear);
      drawTit("Add Assignment", "Return to Week", toWeek);
      frame.setVisible(true);      
   }
   
   public void updateAssignAdder(String extra, String textBox) {
      GridLayout aA = new GridLayout(9, 1);
      JPanel assignAdder = new JPanel(aA);
      assignAdder.add(des);
      assignAdder.add(assignDescrip);
      assignAdder.add(subj);
      assignAdder.add(classOptions);
      assignAdder.add(dD);
      assignAdder.add(dueDate);
      assignAdder.add(ty);
      assignTypePanel = new JPanel(new GridLayout(1, 3));
      assignTypePanel.add(assignType);
      if (extra.equals("paper")) {
         paperType.addActionListener(getAssignType);
         assignTypePanel.add(paperType);
      } else if (extra.equals("project")) {
         projectType.addActionListener(getAssignType);
         assignTypePanel.add(projectType);
      } else if (extra.equals("test")) {
         testType.addActionListener(getAssignType);
         assignTypePanel.add(testType);
      }
      if (!textBox.equals("")) {
         addInfo.setText(textBox);
         assignTypePanel.add(addInfo);
      }
      assignAdder.add(assignTypePanel);
      JButton addAssign = new JButton("Add Assignment");
      addAssign.addActionListener(changeArrayList);
      assignAdder.add(addAssign);
            
      cardPanel.add(assignAdder, "update");
      calCards.show(cardPanel, "update");
      frame.add(cardPanel, BorderLayout.CENTER);
      drawNav("Add or Remove Class", "Change School Year", "Add Assignment", toClass, toYear);
      drawTit("Add Assignment", "Return to Week", toWeek);
      frame.setVisible(true);
   }
   
   public void drawClassAdder() {
      int count = 4;
      GridLayout assignLevels = new GridLayout(12, 1);
      JPanel classAdder = new JPanel(assignLevels);
      classAdder.add(new JLabel("ADD A CLASS"));
      classAdder.add(new JLabel("Class name: "));
      className = new JTextField();
      classAdder.add(className);
      classAdder.add(new JLabel("Class start time in the format hh:mm am/pm: "));
      startTime = new JTextField();
      classAdder.add(startTime);
      classAdder.add(new JLabel("Class end time in the format hh:mm am/pm"));
      endTime = new JTextField();
      classAdder.add(endTime);
      JButton addClass = new JButton("Add Class");
      addClass.addActionListener(changeArrayList);
      classAdder.add(addClass);
      classAdder.add(new JLabel("REMOVE A CLASS"));
      classOptions = new JComboBox(classes.toArray(new Class[classes.size()]));
      classAdder.add(classOptions);
      JButton removeClass = new JButton("Remove Class");
      removeClass.addActionListener(changeArrayList);
      classAdder.add(removeClass);
      
      cardPanel.add(classAdder, "hi");
      calCards.show(cardPanel, "hi");
      frame.add(cardPanel, BorderLayout.CENTER);
      
      drawNav("Change School Year", "Add Assignment", "Add or Remove Class", toYear, toAssignAdder);
      drawTit("Add or Remove Class", "Return to Week", toWeek);
      frame.setVisible(true);
      //getAssignType(descrip.getText(), findClass(subject.getText()), new Day( 
      
   }
   
   public void drawYearChanger() {
      int count = 4;
      GridLayout assignLevels = new GridLayout(9, 1);
      JPanel yearChanger = new JPanel(assignLevels);
      yearChanger.add(new JLabel("New start date in the format mm/dd/yyyy:"));
      startDate = new JTextField();
      yearChanger.add(startDate);
      JButton changeStartDate = new JButton("Change Start Date");
      changeStartDate.addActionListener(changeArrayList);
      yearChanger.add(changeStartDate);
      yearChanger.add(new JLabel("New end date in the format mm/dd/yyyy:"));
      endDate = new JTextField();
      yearChanger.add(endDate);
      JButton changeEndDate = new JButton("Change End Date");
      changeEndDate.addActionListener(changeArrayList);
      yearChanger.add(changeEndDate);
      yearChanger.add(new JLabel("Number of weeks to be added to the end of the school year:"));
      weeksAdded = new JTextField();
      yearChanger.add(weeksAdded);
      JButton addWeeks = new JButton("Add Weeks");
      addWeeks.addActionListener(changeArrayList);
      yearChanger.add(addWeeks);
      
      cardPanel.add(yearChanger, "hi");
      calCards.show(cardPanel, "hi");
      frame.add(cardPanel, BorderLayout.CENTER);
      
      drawNav("Add Assignment", "Add or Remove Class", "Change School Year", toAssignAdder, toClass);
      drawTit("Change School Year", "Return to Week", toWeek);
      frame.setVisible(true);
      //getAssignType(descrip.getText(), findClass(subject.getText()), new Day( 
      
   }

   public void drawNav(String n1, String n2, String tit, Action x, Action y) {
      if (bNCardsNames.indexOf(tit) == -1) {
         JPanel moveButtons = new JPanel(new GridLayout(1, 2));
         JButton pW = new JButton(n1);
         JButton nW = new JButton(n2);
         pW.setFont(new Font("Times New Roman", Font.PLAIN, 20));
         nW.setFont(new Font("Times New Roman", Font.PLAIN, 20));
         pW.setBackground(new Color(102, 178, 255));
         nW.setBackground(new Color(102, 178, 255));
         pW.addActionListener(x);
         nW.addActionListener(y);
         moveButtons.add(pW);
         moveButtons.add(nW);
         botNav.add(moveButtons, tit);
         bNCards.show(botNav, tit);
         bNCardsNames.add(tit);
         frame.add(botNav, BorderLayout.SOUTH); 
      } else {   
         bNCards.show(botNav, tit);
      }
   }
   
   public void drawTit(String tit, String bName, Action a) {
      if (tTCardsNames.indexOf(tit) == -1) {
         JPanel title = new JPanel(new GridLayout(1, 2));
         JLabel t = new JLabel(tit);
         t.setFont(new Font("Times New Roman", Font.PLAIN, 30));
         t.setHorizontalAlignment(SwingConstants.CENTER);
         t.setBackground(new Color(51, 153, 255));
         t.setOpaque(true);
         title.add(t);
         JButton backToWeek = new JButton(bName);
         backToWeek.setFont(new Font("Times New Roman", Font.PLAIN, 20));
         backToWeek.setBackground(new Color(153, 204, 255));
         dayAssign = false;
         backToWeek.addActionListener(a);
         title.add(backToWeek);
         topTit.add(title, tit);
         tTCards.show(topTit, tit);
         frame.add(topTit, BorderLayout.NORTH);
      } else {
         tTCards.show(topTit, tit);
      }
   }
   
   public Class findClass(String name) {
      for (Class c : classes) {
         if (c.name.equalsIgnoreCase(name)) {
            return c;
         }
      }
      return new Class(name, new Time(0), new Time(0));
   }
   
   public String splitRecomb(String[] n, String c) {
      String na = n[0];
      for (int i = 1; i < n.length; i++) {
         na += c + n[i];
      }
      return na;
   }
   
   public void makeYear() {
      schoolYear = new ArrayList<Day[]>();
      Day count = new Day(calStart.absPos, calStart.year);
      for (int i = 0; i <= calStart.daysTo(calEnd) / 7; i ++) {
         Day[] week = new Day[7];
         for (int j = 0; j < 7; j++) {
             week[j] = count;
             count = new Day(count.dayAfter[0], count.dayAfter[1]);
         }
         schoolYear.add(week);
      }
   }
   
   public Assignment getType (String t, String n, Day d, Class c, String s) {
      if (t.equals("Assignment")) {
         return new Assignment(n, d, c);
      } else if (t.equals("Homework")) {
         return new Homework(n, d, c);
      } else if (t.equals("Field Trip")) {
         return new FieldTrip(n, d, c);
      } else if (t.equals("Lab")) {
         return new Lab(n, d, c);
      } else if (t.equals("Paper")) {
         return new Paper(n, d, c);
      } else if (t.equals("Project")) {
         return new Project(n, d, c);
      } else if (t.equals("Poem")) {
         return new Poem(n, d, c);
      } else if (t.equals("Research Paper")) {
         return new ResearchPaper(n, d, c);
      } else if (t.equals("Art Project")) {
         return new ArtProject(n, d, c);
      } else if (t.equals("Poster")) {
         return new Poster(n, d, c);
      } else if (t.equals("Test")) {
         return new Test(n, d, c);
      } else if (t.equals("Final")) {
         return new Final(n, d, c);
      } else if (t.equals("Quiz")) {
         return new Quiz(n, d, c);
      } else if (t.equals("Reading")) {
         return new Reading(n, d, c, Integer.parseInt(s));
      } else if (t.equals("Event")) {
         String[] st = s.split("/");
         Time ti = new Time(Integer.parseInt(st[0]), Integer.parseInt(st[1]), st[2]);
         return new Event(n, d, c, ti);
      } else if (t.equals("Short Story")) {
         return new ShortStory(n, d, c, Integer.parseInt(s));
      } else if (t.equals("Essay")) {
         return new Essay(n, d, c, Integer.parseInt(s));
      } else if (t.equals("Slide Show")) {
         return new SlideShow(n, d, c, Integer.parseInt(s));
      } else if (t.equals("Vocab")) {
         return new Vocab(n, d, c, Integer.parseInt(s));
      }
      return new Assignment(n, d, c);
   }     
   
   public class Switch extends AbstractAction {
      public Switch() {
         super();
      }
      
      public void actionPerformed (ActionEvent e) {
         if (e.getActionCommand().equals("Previous Week")) {
            if (showWeek > 0) {
               drawWeekCal(showWeek - 1);
            }
         } else if (e.getActionCommand().equals("Next Week")){
            if (showWeek < schoolYear.size() - 1) {
               drawWeekCal(showWeek + 1);
            }
         } else if (e.getActionCommand().equals("Previous Day")) {
            if (showDay.compareTo(schoolYear.get(0)[0]) > 0) {
               drawDayCal(new Day(showDay.dayBefore[0], showDay.dayBefore[1]));
            }
         } else if (e.getActionCommand().equals("Next Day")){
            if (showDay.compareTo(schoolYear.get(schoolYear.size() - 1)[6]) < 0) {
               drawDayCal(new Day(showDay.dayAfter[0], showDay.dayAfter[1]));
            }
         }
      }
   }
      
   public class ViewTo extends AbstractAction {
      String type;
      public ViewTo(String t) {
         super();
         type = t;
      }
      
      public void actionPerformed (ActionEvent e) {
         if (type.equals("day")) {
            String[] nums = e.getActionCommand().split("/");
            Day aDay = new Day(Integer.parseInt(nums[0]), Integer.parseInt(nums[1]), Integer.parseInt(nums[2]));
            drawDayCal(aDay);
         } else if (type.equals("week")) {
            if(dayAssign) {
               Day d = new Day(showDay.firstDayOfWeek[0], showDay.firstDayOfWeek[1]);
               for (int i = 0; i < schoolYear.size(); i++) {
                  if (schoolYear.get(i)[0].equals(d)) {
                     drawWeekCal(i);
                     break;
                  }
               }
            } else {
               drawWeekCal(showWeek);
            }
         } else if (type.equals("assignAdder")) {
            drawAssignAdder();
         } else if (type.equals("class")) {
            drawClassAdder();
         } else if (type.equals("year")) {
            drawYearChanger();
         }
      }
   }   
   
   public class AddOrRemove extends AbstractAction {
      public AddOrRemove() {
         super();
      }
      
      public void actionPerformed (ActionEvent e) {
         if (e.getActionCommand().equals("Remove Class")) {
            Class selectedClass = (Class)classOptions.getSelectedItem();
            classes.remove(selectedClass);
            drawClassAdder();
         } else if (e.getActionCommand().equals("Add Class")) {
            String[] sT = startTime.getText().split(" ");
            String[] s = sT[0].split(":");
            String[] eT = endTime.getText().split(" ");
            String[] en = eT[0].split(":");
            Time start = new Time(Integer.parseInt(s[0]), Integer.parseInt(s[1]), sT[1]);
            Time end = new Time(Integer.parseInt(en[0]), Integer.parseInt(en[1]), eT[1]);
            classes.add(new Class(className.getText(), start, end));
            Collections.sort(classes);
            drawClassAdder();
         } else if (e.getActionCommand().equals("Add Weeks")) {
            int x = Integer.parseInt(weeksAdded.getText());
            for (int i = 0; i < x; i++) {
               Day[] week = new Day[7];
               for (int j = 0; j < 7; j++) {
                  week[j] = calEnd;
                  calEnd = new Day(calEnd.dayAfter[0], calEnd.dayAfter[1]);
               }
               schoolYear.add(week);
            }
            drawWeekCal(showWeek);
         } else if (e.getActionCommand().equals("Change Start Date")) {
            String s = startDate.getText();
            String[] sp = s.split("/");
            Day dateStart = new Day(Integer.parseInt(sp[0]), Integer.parseInt(sp[1]), Integer.parseInt(sp[2]));
            calStart = new Day(dateStart.firstDayOfWeek[0], dateStart.firstDayOfWeek[1]);
            makeYear();
            showWeek = 0;
            drawWeekCal(showWeek);
         } else if (e.getActionCommand().equals("Change End Date")) {
            String s = endDate.getText();
            String[] sp = s.split("/");
            Day dateEnd = new Day(Integer.parseInt(sp[0]), Integer.parseInt(sp[1]), Integer.parseInt(sp[2]));
            calEnd = new Day(dateEnd.firstDayOfWeek[0], dateEnd.firstDayOfWeek[0]);
            calEnd  = new Day(calEnd.add(6)[0], calEnd.add(6)[0]);
            makeYear();
            showWeek = 0;
            drawWeekCal(showWeek);
         } else if (e.getActionCommand().equals("Add Assignment")) {
            String[] sD = dueDate.getText().split("/");
            Day d = new Day(Integer.parseInt(sD[0]), Integer.parseInt(sD[1]), Integer.parseInt(sD[2]));
            Class c = (Class)classOptions.getSelectedItem();
            String n = assignDescrip.getText();
            String s = addInfo.getText();
            assigns.add(getType(thisAssignType, n, d, c, s));
            drawWeekCal(showWeek);
         } 
      }
   }
   
   public class AddChoice extends AbstractAction {
      public AddChoice() {
         super();
      }
      
      public void actionPerformed (ActionEvent e) {
         JComboBox aBox = (JComboBox)e.getSource();
         String first = (String)aBox.getItemAt(0);
         String select = (String)aBox.getSelectedItem();
         thisAssignType = select;
         if (first.equals("Assignment")) {
            if (select.equals("Assignment") || select.equals("Homework") || select.equals("Lab") || select.equals("Field Trip")) {
               updateAssignAdder("", "");
            } else if (select.equals("Event")) {
               updateAssignAdder("", "Please enter event start time (in format hh mm am/pm)");
            } else if (select.equals("Reading")) {
               updateAssignAdder("", "Please enter number of chapters to be read");
            } else if (select.equals("Paper")) {
               updateAssignAdder("paper", "");
            } else if (select.equals("Test")) {
              updateAssignAdder("test", "");
            } else if (select.equalsIgnoreCase("project")) {
               updateAssignAdder("project", "");
            }
         } else if (first.equals("Project")){
            if (select.equals("Art Project") || select.equals("Poster")) {
               updateAssignAdder("project", "");
            } else if (select.equals("Slide Show")) {
               updateAssignAdder("project", "Please enter number of slides needed");
            }
         } else if (first.equals("Paper")) {
            if (select.equals("Poem") || select.equals("Research Paper")) {
               updateAssignAdder("paper", "");
            } else if (select.equals("Essay") || select.equals("Short Story")) {
               updateAssignAdder("paper", "Please enter required number of pages");
            }
         } else if (first.equals("Test")){
            if (select.equals("Final") || select.equals("Quiz")) {
               updateAssignAdder("test", "");
            } else if (select.equals("Vocab")) {
               updateAssignAdder("test", "Please enter required number of pages");
            }
         }
      }
   }
}