import java.awt.*;

public class Lab extends Assignment {
   
   public Lab (String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //this.reminderDate = getReminderDate();
      //type = "Lab";
      //makeReminder();
   }
   
   public Color getColor() {
      return new Color(255, 162, 0);
   }
   
   public String setType() {
      return "Lab";
  }
   
  public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(2)[0], d.subtract(2)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }  
      
   public String toString() {
      return "Lab " + name + " due";
  }

}