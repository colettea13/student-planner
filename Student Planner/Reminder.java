import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.TimerTask.*;
import java.util.*;
import java.util.Timer.*;


public class Reminder {
    Timer timer;
    String name;

    public Reminder(String n, DayTime dt) {
        Day d = dt.d;
        Calendar cal = Calendar.getInstance();
        cal.set(d.year, d.monthNum, d.monthPos, (int)dt.t.absTime, dt.t.minute);
        name = n;
        timer = new Timer();
        timer.schedule(new RemindTask(this.name), cal.getTime());
        
	}

    class RemindTask extends TimerTask {
        
        private String name;
        
        public RemindTask (String name){
            this.name = name;
        }
        
        public void run() {
            JOptionPane.showMessageDialog(null, name);
            timer.cancel(); //Terminate the timer thread
        }
    }

}