public class Poem extends Paper {
  
   public Poem (String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      type = "Poem";
      //makeReminder();
   }
   
   public String setType() {
      return "Poem";
  }
   
   public String toString() {
      return "Poem " + name + " due";
   }
}
