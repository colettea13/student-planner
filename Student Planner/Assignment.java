import java.util.*;
import java.text.*;
import java.awt.*;

public class Assignment implements Comparable{

   
   public String name;
   public Class subject;
   public Day dueDate;
   public Day reminderDate;
   public Day assignDate;
   public Time reminderTime;
   public DayTime reminderDateTime;
   public String type;
   public String extra;
   public Color col;

   

   public Assignment(String n, Day dD, Class s) {
      type = setType();
      name = n;
      subject = s;
      dueDate = dD;
      extra = "*";
      col = getColor();
      assignDate = getToday();
      reminderDate = getReminderDate();
      reminderTime = new Time(5, 00, false);
      makeReminder();
      makeExtraReminder();
   }
   
   public void makeReminder() {
      if (getToday().compareTo(dueDate) <= 0) {
         //System.out.println(getToday().compareTo(dueDate) + name);
         //System.out.println(type + " " + name + " " + reminderDate + " " + reminderDate.compareTo(assignDate) + " " + dueDate);
         reminderDateTime = new DayTime(reminderDate, reminderTime);
         Reminder reminder = new Reminder(type + " for " + subject.name + ":\n" + name + "\nDue: " + dueDate, reminderDateTime);
      }
   }
   
   public void makeExtraReminder() {
      if (dueDate.compareTo(reminderDate) > 1) {
         DayTime r2DateTime = new DayTime(new Day(dueDate.dayBefore[0], dueDate.dayBefore[1]), reminderTime);
         Reminder reminder2 = new Reminder(type + " for " + subject.name + ":\n" + name + "\nDue TOMORROW", r2DateTime);
      }
   }
   
   public Color getColor() {
      return new Color(255, 255, 153);
   }
   
   public Day getToday() {
      DateFormat dateFormat = new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
      DateFormat yearString = new SimpleDateFormat("yyyy");
      DateFormat monthString = new SimpleDateFormat("MM");
      DateFormat dayString = new SimpleDateFormat("dd");
      Calendar cal = Calendar.getInstance();
      Day today = new Day (Integer.parseInt(monthString.format(cal.getTime())),
                          Integer.parseInt(dayString.format(cal.getTime())),
                          Integer.parseInt(yearString.format(cal.getTime())));

      return today;
   }

   public String toString() {
      return name + " due";
  }
  
  public void setReminderDate(Day rDate) {
      this.reminderDate = rDate;
  }
  
  public String setType() {
      return "Assignment";
  }
  
  public Day getReminderDate() {
      Day d = new Day(dueDate.dayBefore[0], dueDate.dayBefore[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d;
  }
  
  public int compareTo(Object other) {
    Assignment o = (Assignment)other;
    int dayCompare = dueDate.compareTo(o.dueDate);
    if (dayCompare != 0) {
      return dayCompare;
    } else {
      return subject.compareTo(o.subject);
    }
  }

  public boolean equals(Object other) {
    Assignment o = (Assignment)other;
    return name.equals(o.name) && dueDate.equals(o.dueDate) &&
           reminderDate.equals(o.reminderDate) && assignDate.equals(o.assignDate) &&
           reminderTime.equals(o.reminderTime);
  }
  
}
