import java.util.*;
import java.text.*;

public class Day implements Comparable {
   public int year, absPos, monthNum, monthPos, weekNum, weekMonthNum, weekPos, daysInYear;
   public int[] dayBefore, dayAfter, firstDayOfWeek, firstDayOfMonth, firstDayOfYear;
   public String weekDay, month, dateString;
   private int[] monthDays = {31,28,31,30,31,30,31,31,30,31,30,31};
   private String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
   private String[] dayNames = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
   public static final int startYear = 2015;
   
   public Day (int abs, int y) {
      absPos = abs;
      year = y;
      calcMonthWeek();
      initializeVariables();
   }
   
   public Day (int m, int mP, int y) {
      year = y;
      monthNum = m - 1;
      monthPos = mP;
      calcAbs(m, mP);
      initializeVariables();
   }
   
   /*public Day (Day other, String ind, String wP, int y) {
      year = y;
      String weekDay = wP;
   }*/
   
   public void initializeVariables() {
      DateFormat dFormat = new SimpleDateFormat("EEE MM/dd/yyyy");
      Calendar cal = Calendar.getInstance();
      cal.set(year, monthNum, monthPos);
      daysInYear = (int)new Integer(cal.getActualMaximum(Calendar.DAY_OF_YEAR));
      daysInYear = 365;
      if (daysInYear == 366) {
         monthDays[1] = 29;
      }
      month = monthNames[monthNum];
      dateString = dFormat.format(cal.getTime());
      dayOfWeek();
      setOtherDays();
   }
   
   public void setOtherDays() {
      dayBefore = subtract(1);
      dayAfter = add(1);
      firstDayOfWeek = subtract(weekPos - 1);
      firstDayOfMonth = subtract(monthPos - 1);
      firstDayOfYear = subtract(absPos - 1);
   }
   
   private int calcAbs(int m, int mp) {
      int absDay = 0;
      for (int i = 0; i < m - 1; i++) {
         absDay += monthDays[i];
      }
      absDay += mp;
      absPos = absDay;
      return absDay;
   }

   private void calcMonthWeek() {
      int monthCount = 0;
      int temp = absPos;
      while (temp > monthDays[monthCount]) {
         temp -= monthDays[monthCount];
         monthCount++;
      }
      monthNum = monthCount;
      monthPos = temp;
   }
   
   public void dayOfWeek() {
      weekDay = dateString.substring(0, 3);
      for (int i = 1; i <= 7; i++) {
         if (weekDay.equals(dayNames[i-1].substring(0,3))) {
            weekPos = i;
         }
      }
   }
   
   public int[] add(int num) {
      if (absPos + num > daysInYear) {
         int[] ret = {(absPos + num) - daysInYear, year + 1};
         return ret;
      } else {
         int[] ret = {absPos + num, year};
         return ret;
      }
   }
   
   public int[] subtract(int num) {
      if (absPos - num <= 0) {
         int[] ret = {daysInYear + (absPos - num), year - 1};
         return ret;
      } else {
         int[] ret = {absPos - num, year};
         return ret;
      }
   }    
   
   public int daysTo(Day other) {
      int daysTo = 0;
      if (other.absPos - absPos >= 0) {
         daysTo = other.absPos - absPos;
      } else {
         daysTo = other.absPos + (365 - absPos);
      }
      return daysTo;
   }
   
   public String toString() {
      return monthNum + 1 + "/" + monthPos + "/" + year;
   }
   
   public int compareTo(Object other) {
      Day o = (Day)other;
      int absValThis = absPos + ((year - startYear) * 365);
      int absValOther = o.absPos + ((o.year - startYear) * 365);
      return absValThis - absValOther;
   }
   
   public boolean equals(Object other) {
      Day o = (Day)other;
      int absValThis = absPos + ((year - startYear) * 365);
      int absValOther = o.absPos + ((o.year - startYear) * 365);
      return absValThis == absValOther;
   }
}