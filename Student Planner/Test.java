import java.awt.*;

public class Test extends Assignment {

   public Test(String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //this.reminderDate = getReminderDate();
      //type = "Test";
   }
   
   public Color getColor() {
      return new Color(255, 100, 100);
   }
   
   public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(4)[0], d.subtract(4)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String setType() {
      return "Test";
  }
   
   public String toString() {
      return "Test " + super.name;
   }
}