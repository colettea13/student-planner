import java.util.*;
import java.awt.*;
import java.applet.*; 
import java.awt.event.*;
import java.text.*;

public class DrawCalendar {
   public ArrayList<Day[]> month;
   public Day[] week;
   public Day thisDay;
   public ArrayList<Assignment> assigns;
   public ArrayList<Class> classes;
   public String[][] weekGrid;
   public Color[][] weekColor;
   public ArrayList<String> dayInfo;
   public ArrayList<Color> dayColor;
   public boolean[][] isToday;
   public boolean dayIsToday;
   
   public DrawCalendar(Day[] w, ArrayList<Assignment> a, ArrayList<Class> c) {
      week = w;
      assigns = a;
      classes = c;
      weekGrid = new String[classes.size() + 1][8];
      weekColor = new Color[classes.size() + 1][8];
      isToday = new boolean[classes.size() + 1][8];
      int count = 1;
      weekGrid[0][0] = "";
      weekColor[0][0] = new Color(224, 224, 224);
      Day today = getToday();
      for (int i = 0; i <= classes.size(); i++) {
         for (int j = 1; j < 8; j++) {
            weekGrid[i][j] = "";
            weekColor[i][j] = new Color(224, 224, 224);
            Day d = week[j - 1];
            if (d.equals(today)) {
               isToday[i][j] = true;
            }
         }
      }
      for (Class cla : classes) {
         weekGrid[count][0] = cla.name;
         //weekColor[count][0] = cla.col;
         weekColor[count][0] = new Color(76, 153, 0);
         count++;
      }
      count = 1;
      for (Day d : week) {
         weekGrid[0][count] = d.weekDay;
         weekColor[0][count] = new Color(204, 255, 153);
         count++;
      }
      weekColor[0][1] = new Color(229, 255, 204);
      weekColor[0][7] = new Color(229, 255, 204);
      for (Assignment assign : assigns) {
         for (int i = 0; i < 7; i++) {
            Day d = week[i];
            if (assign.dueDate.equals(d)) {
               for (int j = 0; j < classes.size(); j++) {
                  Class cla = classes.get(j);
                  if (assign.subject.equals(cla)) {
                     weekGrid[j + 1][i + 1] += assign.type + ", ";
                     weekColor[j+1][i+1] = assign.col;
                  }
               }
            }
         }
      }
   }
   
   
   public DrawCalendar(Day d, ArrayList<Assignment> a) {
      thisDay = d;
      if (thisDay.equals(getToday())) {
         dayIsToday = true;
      }
      assigns = a;
      dayInfo = new ArrayList<String>();
      dayColor = new ArrayList<Color>();
      dayInfo.add("Assignments due on " + thisDay + ", ");
      dayColor.add(new Color(204, 255, 153));
      boolean hasAssign = false;
      for (Assignment assign : assigns) {
         if (assign.dueDate.equals(thisDay)) {
            dayInfo.add(assign.type + " for " + assign.subject.name + ": " + assign.name);
            //dayInfo.add(assign.toString());
            dayColor.add(assign.col);
            hasAssign = true;
         }
      }
      if (!hasAssign) {
         dayInfo.add("There are no assignments due on this day");
         dayColor.add(new Color(224, 224, 224));
      }
   }
   
   public Day getToday() {
      DateFormat dateFormat = new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
      DateFormat yearString = new SimpleDateFormat("yyyy");
      DateFormat monthString = new SimpleDateFormat("MM");
      DateFormat dayString = new SimpleDateFormat("dd");
      Calendar cal = Calendar.getInstance();
      Day today = new Day (Integer.parseInt(monthString.format(cal.getTime())),
                          Integer.parseInt(dayString.format(cal.getTime())),
                          Integer.parseInt(yearString.format(cal.getTime())));

      return today;
   }
}