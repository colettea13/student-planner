import java.util.*;

public class Time implements Comparable{
   public int hour;
   public int minute;
   public String mornNight = "PM";
   public boolean ampm = false;
   public double absTime;
   
   public Time (int h, int m, boolean ap) {
      hour = h;
      minute = m;
      ampm = ap;
      if (ampm) {
         mornNight = "AM";
      }
      calcAbsTime();
   }
   
   public Time (int h, int m, String ap) {
      hour = h;
      minute = m;
      mornNight = ap.toUpperCase();
      if (mornNight.equals("AM")) {
         ampm = true;
      }
      calcAbsTime();
   }
   
   public Time (double aT) {
      absTime = aT;
      calcHourMin();
   }
   
   public void calcAbsTime() {
      if (hour != 12) {
         if (ampm) {
            absTime = hour + (minute / 60);
         } else {
            absTime = hour + 12 + (minute / 60);
         }
      } else {
         if (ampm) {
            absTime = (minute / 60);
         } else {
            absTime = hour + (minute / 60);
         }
      }
   }
   
   public void calcHourMin() {
      ampm = hour < 12;
      if (hour <= 12) {
         hour = (int) absTime;
         minute = (int)(60 * (absTime - hour));
      } else {
         hour = (int) absTime - 12;
         minute = (int)Math.round(60 * (absTime - 12 - hour));
      }
   }
   
   public String toString() {
      return hour + ":" + minute + " " + mornNight;
   }
   
   public int compareTo(Object other) {
      Time o = (Time)other;
      return (int)((absTime - o.absTime)* 60);
   }
   
   public boolean equals (Object other) {
      Time o = (Time)other;
      return absTime == o.absTime;
   }
}