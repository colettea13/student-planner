public class Final extends Test {
   
   public Final (String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //this.reminderDate = getReminderDate();
      //type = "Final";
      //makeReminder();
   }
   
   public String setType() {
      return "Final";
  }
   
  public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(10)[0], d.subtract(10)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String toString() {
      return "Final " + name;
   }
}
