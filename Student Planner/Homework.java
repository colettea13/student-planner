import java.awt.*;

public class Homework extends Assignment {
  
  public Homework (String name, Day dueDate, Class subject) {
     super(name, dueDate, subject);
     //type = "Homework";
     //makeReminder();
  }
  
  public Color getColor() {
   //return new Color(66, 212, 209);
   return new Color(102, 255, 255);
  }
  
  public String setType() {
      return "Homework";
  }
  
  public String toString() {
     return "Homework " + super.name + " due";
  }
  
}
