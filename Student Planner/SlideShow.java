public class SlideShow extends Project {
   public int slideNum;
   
   public SlideShow (String name, Day dueDate, Class subject, int sNum) {
      super(name, dueDate, subject);
      slideNum = sNum;
      extra = sNum + "";
   }
   
   public String setType() {
      return "SlideShow";
  }
   
   public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(slideNum/3)[0], d.subtract(slideNum/3)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String toString() {
      return "Slide show " + name + " due";
   }
}
