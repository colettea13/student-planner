import java.util.*;
import java.awt.*;

public class Class implements Comparable{
   public String name;
   public Time startTime, endTime;
   public Color col;
   
   public Class (String className, Time sTime, Time eTime) {
      name = className;
      startTime = sTime;
      endTime = eTime;
      col = new Color((int)(Math.random() * 255),
                      (int)(Math.random() * 255),
                      (int)(Math.random() * 255));
   }
   
   /*public String toString() {
      String ret = "[";
      if (assignments.size() > 0) {
         ret += assignments.get(0);
         for (int i = 1; i < assignments.size(); i++) {
            ret += ", " + assignments.get(i);
         }
      }
      return ret + "]";
   }*/
   
   public String toString() {
      return name + " (" + startTime + " - " + endTime + ")" ;
   }
   
   public int compareTo(Object other) {
      Class o = (Class)other;
      return startTime.compareTo(o.startTime);
   }
   
   public boolean equals(Object other) {
      Class o = (Class)other;
      return name.equals(o.name) && startTime.equals(o.startTime) && endTime.equals(o.endTime);
   }
}