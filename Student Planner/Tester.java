import java.util.*;
import java.awt.*;
import java.text.*;

public class Tester {
   public static void main (String[] args) {
      ArrayList<Day[]> schoolYear = new ArrayList<Day[]>();
      ArrayList<Class> classes = new ArrayList<Class>();
      ArrayList<Assignment> assigns = new ArrayList<Assignment>();
      Scanner input = new Scanner(System.in);
      System.out.println("Please enter your classes:");
      String done = "";
      while (!done.startsWith("n") && !done.startsWith("N")) {
         System.out.print("Please enter class name: ");
         String className = input.next();
         Time startTime = getTime(input, "start");
         Time endTime = getTime(input, "end");
         classes.add(new Class(className, startTime, endTime));
         System.out.print("Would you like to add another class? (please type \"yes\" or \"no\"): ");
         done = input.next();
      }
      //System.out.print("Please enter your school end date in the format mm dd yyyy: ");
      //Day endDate = new Day(input.nextInt(), input.nextInt(), input.nextInt());
      Day endDate = new Day(6, 16, 2015);
      DateFormat dateFormat = new SimpleDateFormat("EEE yyyy/MM/dd HH:mm:ss");
      Calendar cal = Calendar.getInstance();
      String dateString = dateFormat.format(cal.getTime());
      System.out.println(cal.getTimeInMillis());
      Day today = new Day (Integer.parseInt(dateString.substring(9, 11)),
                           Integer.parseInt(dateString.substring(12, 14)),
                           Integer.parseInt(dateString.substring(4, 8)));
      //today = new Day(9, 7, 2014);
      //System.out.println(today);
      //System.out.println(dateFormat.format(cal.getTime())); //Tue 2015/05/05 08:15:34
      Day calStart = new Day(today.firstDayOfWeek[0], today.firstDayOfWeek[1]);
      Day calEnd = new Day(endDate.firstDayOfWeek[0], endDate.firstDayOfWeek[0]);
      calEnd = new Day(calEnd.add(6)[0], calEnd.add(6)[1]);
      Day count = new Day(calStart.absPos, calStart.year);
      for (int i = 0; i <= calStart.daysTo(calEnd) / 7; i ++) {
         Day[] week = new Day[7];
         for (int j = 0; j < 7; j++) {
             week[j] = count;
             count = new Day(count.dayAfter[0], count.dayAfter[1]);
         }
         schoolYear.add(week);
      }
      System.out.println("Please enter your assignments:\n");
      done = "";
      while (!done.startsWith("n") && !done.startsWith("N")) {
         System.out.print("Please enter assignment description: ");
         String descrip = input.next();
         System.out.print("Please enter the class this assingment is for: ");
         String className = input.next();
         Class assignClass = findClass(className, classes);
         System.out.print("Please enter the due date of the assignment in the format mm dd yyyy: ");
         Day dueDate = new Day(input.nextInt(), input.nextInt(), input.nextInt());
         assigns.add(getAssignType(input, descrip, assignClass, dueDate));
         System.out.print("Would you like to add another assignment? (please type \"yes\" or \"no\"): ");
         done = input.next();
      }
      //classes.add(new Class("Pre-Calc", new Time(7, 50, true), new Time(8, 45, true)));
      //classes.add(new Class("LA10", new Time(8, 50, true), new Time(9, 45, true)));
      classes.add(new Class("APHG", new Time(9, 50, true), new Time(10, 45, true)));
      classes.add(new Class("APCS", new Time(11, 25, true), new Time(12, 20, false)));
      classes.add(new Class("Bio", new Time(12, 25, false), new Time(1, 20, false)));
      classes.add(new Class("French", new Time(1, 25, false), new Time(2, 20, false)));
      
      assigns.add(new ArtProject("collage on imagery from book quote", new Day(5, 20, 2015), classes.get(1)));
      assigns.add(new Assignment("got to help Helena w/ poem", new Day(5, 21, 2015), classes.get(1)));
      assigns.add(new Essay("compare/contrast - SH5 and Allegory Of The Cave Stephen Dunn", new Day(5, 20, 2015), classes.get(1), 2));
      assigns.add(new Event("performing in assembly", new Day(5, 22, 2015), classes.get(2), new Time(10, 00, true)));
      assigns.add(new FieldTrip("going to microsoft, bring notebook", new Day(5, 26, 2015), classes.get(3)));
      assigns.add(new Final("math final", new Day(5, 29, 2015), classes.get(0)));
      assigns.add(new Homework("math problems 1-5", new Day(5, 18, 2015), classes.get(0)));
      assigns.add(new Lab("cs lab w/ graphics", new Day(5, 19, 2015), classes.get(3)));
      assigns.add(new Paper("write up for movie", new Day(5, 25, 2015), classes.get(2)));
      assigns.add(new Poem("must be inspired by SH5 quote", new Day(5, 25, 2015), classes.get(1)));
      assigns.add(new Poster("grasshopper anatomy", new Day(5, 28, 2015), classes.get(4)));
      assigns.add(new Project("create star - Little Prince", new Day(5, 21, 2015), classes.get(5)));
      assigns.add(new Quiz("on subjonctif", new Day(5, 27, 2015), classes.get(5)));
      assigns.add(new Reading("SH5 chap 7 & 8", new Day(5, 29, 2015), classes.get(1), 2));
      assigns.add(new ResearchPaper("why plants grow - cr + photo", new Day(5, 28, 2015), classes.get(4)));
      assigns.add(new ShortStory("10 vignettes from vacation", new Day(5, 26, 2015), classes.get(1), 3));
      assigns.add(new SlideShow("teach sector model", new Day(5, 20, 2015), classes.get(2), 6));
      assigns.add(new Test("test on city models", new Day(5, 27, 2015), classes.get(2)));
      assigns.add(new Vocab("words on happiness", new Day(5, 22, 2015), classes.get(1), 20));
      
      Collections.sort(classes);
      Collections.sort(assigns);
      System.out.println(classes);
      for (Day[] week : schoolYear) {
         System.out.print("[");
         for (Day printDay : week) {
            System.out.print(printDay + ", ");
         }
         System.out.println("]");
      }
      System.out.println(assigns);
      DrawCalendar day = new DrawCalendar(schoolYear.get(1)[3], assigns);
   }
   
   public static Time getTime(Scanner input, String se) {
      System.out.print("Please enter class " + se + " time in the format hh mm am/pm: ");
      int hour = input.nextInt();
      int min = input.nextInt();
      String mornNight = input.next();
      boolean ampm = true;
      if (mornNight.equalsIgnoreCase("pm")) {
         ampm = false;
      }
      return new Time(hour, min, ampm);
   }   
   
   public static Class findClass(String name, ArrayList<Class> classes) {
      for (Class c : classes) {
         if (c.name.equalsIgnoreCase(name)) {
            return c;
         }
      }
      return new Class(name, new Time(0), new Time(0));
   }
   
   public static Assignment getAssignType(Scanner input, String descrip, Class assignClass, Day dueDate) {
      System.out.print("What type of assignment is this(lab, paper, field-trip, event, reading, test, project, or just plain hw)? ");
      String iniType = input.next();
      if (iniType.equalsIgnoreCase("lab")) {
         return new Lab(descrip, dueDate, assignClass);
      } else if (iniType.equalsIgnoreCase("paper")) {
         System.out.print("What sort of paper is it (research-paper, essay, short-story, poem, or just plain paper)? ");
         String papType = input.next();
         if (papType.equalsIgnoreCase("research-paper")) {
            return new ResearchPaper(descrip, dueDate, assignClass);
         } else if (papType.equalsIgnoreCase("short-story")) {
            System.out.print("About how many pages do you need to write? ");
            return new ShortStory(descrip, dueDate, assignClass, input.nextInt());
         } else if (papType.equalsIgnoreCase("essay")) {
            System.out.print("About how many pages do you need to write? ");
            return new Essay(descrip, dueDate, assignClass, input.nextInt());
         } else if (papType.equalsIgnoreCase("poem")) {
            return new Poem(descrip, dueDate, assignClass);
         } else {
            return new Paper(descrip, dueDate, assignClass);
         }
      } else if (iniType.equalsIgnoreCase("field trip")) {
         return new FieldTrip(descrip, dueDate, assignClass);
      } else if (iniType.equalsIgnoreCase("event")) {
         System.out.print("WHat time does it start (in format hh mm am/pm)? ");
         Time eTime = new Time(input.nextInt(), input.nextInt(), input.next());
         return new Event(descrip, dueDate, assignClass, eTime);
      } else if (iniType.equalsIgnoreCase("reading")) {
         System.out.print("How many chapters do you need to read? ");
         return new Reading(descrip, dueDate, assignClass, input.nextInt());
      } else if (iniType.equalsIgnoreCase("test")) {
         System.out.print("What sort of test is it (quiz, final, vocab, or plain test)? ");
         String testType = input.next();
         if (testType.equalsIgnoreCase("quiz")) {
            return new Quiz(descrip, dueDate, assignClass);
         } else if (testType.equalsIgnoreCase("final")) {
            return new Final(descrip, dueDate, assignClass);
         } else if (testType.equalsIgnoreCase("vocab")) {
            System.out.print("About how many words do you need to study? ");
            return new Vocab(descrip, dueDate, assignClass, input.nextInt());
         } else {
            return new Test(descrip, dueDate, assignClass);
         }
      } else if (iniType.equalsIgnoreCase("project")) {
         System.out.print("What sort of project is it (slide-show, poster, artistic, or plain project)? ");
         String projType = input.next();
         if (projType.equalsIgnoreCase("poster")) {
            return new Poster(descrip, dueDate, assignClass);
         } else if (projType.equalsIgnoreCase("artistic")) {
            return new ArtProject(descrip, dueDate, assignClass);
         } else if (projType.equalsIgnoreCase("slide-show")) {
            System.out.print("About how many slides do you need to make? ");
            return new SlideShow(descrip, dueDate, assignClass, input.nextInt());
         } else {
            return new Project(descrip, dueDate, assignClass);
         }
      }  else {
         return new Homework(descrip, dueDate, assignClass);
      }
   }
}