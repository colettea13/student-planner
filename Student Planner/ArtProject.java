public class ArtProject extends Project {
   
   public ArtProject (String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //this.reminderDate = getReminderDate();
      //type = "Artistic Project";
      //makeReminder();
  }
   
   public String setType() {
      return "Art Project";
  }
   
   public Day getReminderDate() {
      Day d = new Day(dueDate.subtract(4)[0], dueDate.subtract(4)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String toString() {
      return "Art Project " + name + " due";
   }
}
