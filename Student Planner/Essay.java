public class Essay extends Paper {
   
   public int pageNumber;
   
   public Essay (String name, Day dueDate, Class subject, int pNumber){
      super(name, dueDate, subject);
      pageNumber = pNumber;
      extra = pNumber + "";
   }
   
   public String toString() {
      return "Essay " + name + " due";
   }
   
   public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(pageNumber)[0], d.subtract(pageNumber)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d;
   }
   
   public String setType() {
      return "Essay";
  }
   
   
}
