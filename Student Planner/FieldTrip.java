import java.awt.*;

public class FieldTrip extends Assignment {

   public FieldTrip(String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //type = "Field Trip";
      //makeReminder();
   }
   
   public Color getColor() {
      return new Color(255, 238, 3);
   }
   
   public String setType() {
      return "Field Trip";
  }

   public String toString() {
      return "Field trip " + super.name;
   }
}
