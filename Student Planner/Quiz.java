public class Quiz extends Test {

   public Quiz(String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //this.reminderDate = getReminderDate();
      //type = "Quiz";
      //makeReminder();
   }
   
   public String setType() {
      return "Quiz";
  }
   
   public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.dayBefore[0], d.dayBefore[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String toString() {
      return "Quiz " + super.name;
   }
}
