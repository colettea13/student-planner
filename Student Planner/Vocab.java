public class Vocab extends Test {

   public int words;

   public Vocab(String name, Day dueDate, Class subject, int words) {
      super(name, dueDate, subject);
      this.words = words;
      extra = words + "";
   }

   public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(words/10)[0], d.subtract(words/10)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String setType() {
      return "Vocab";
  }
   
   public String toString() {
      return "Vocab test on " + words + " words\n" + name;
  }

}
