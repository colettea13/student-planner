public class ResearchPaper extends Paper {
   
   public ResearchPaper (String name, Day dueDate, Class subject) {
      super(name, dueDate, subject);
      //this.reminderDate = getReminderDate();
      //type = "Research Paper";
      //makeReminder();
   }
   
   public String setType() {
      return "Research Paper";
  }
   
   public Day getReminderDate() {
      Day d = this.dueDate;
      d = new Day(d.subtract(7)[0], d.subtract(7)[1]);
      if (d.compareTo(assignDate) < 0) {
         int diff = dueDate.compareTo(assignDate) / 2;
         d = new Day(assignDate.add(diff)[0], assignDate.add(diff)[1]);
      }
      return d; 
   }
   
   public String toString() {
      return "Research paper " + name + " due";
   }
}
