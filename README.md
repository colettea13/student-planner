# README #

### Student Planner###

creates a versatile planner that provides reminders and allows you to create and view different classes and assignments in an easy-to-use, easy-to-view format

### Set up ###

run jTester
Everything myst be typed in the format specified for the program to work - there is no error handling
There are some commands to type into the console.
To save your information and be able to reuse it, you must type close into the console to close the program - if you close it manually, you lose all your info

### Additional info ###

Tester, SimpleTester, and GUITest are irrelevant, except to show our thought process.
Colette, Maya, Marli, Jordan, Ginger, and Henry are all valid names to type with data already saved to them. Colette has the most complete data

### Who do I talk to? ###

Colette Felton and Maya Pagel
email colettea13@gmail.com w/ questions